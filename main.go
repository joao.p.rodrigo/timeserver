package main

import (
	"net/http"
	"os"
	"time"
)

const DEFAULT_ADDRESS = "0.0.0.0:5535"

func main() {
	mux := http.NewServeMux()

	address := DEFAULT_ADDRESS

	if len(os.Args) > 1 {
		address = os.Args[1]
	}

	mux.HandleFunc("/time", getTime)

	server := &http.Server{Addr: address, Handler: mux}
	server.ListenAndServe()
}

func getTime(w http.ResponseWriter, r *http.Request) {
	ctime := time.Now().Format(time.RFC3339)

	w.Write([]byte(ctime))
}
